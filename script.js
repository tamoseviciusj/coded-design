$(".hamburger").click(function(){
    if($('ul').is(':hidden')){
        $('ul').show(200);
        $('nav').addClass("white");
        $(".hamburger, .gone:nth-child(1)").addClass('blacken');
    }else{
        $('ul').hide(200);
        $('nav').removeClass("white");
        $(".hamburger, .gone:nth-child(1)").removeClass('blacken');
    }
});
$(window).scroll(function () {
    var $this = $(this),
        $head = $('nav');
    if ($this.scrollTop() > 1) {
       $head.addClass('navchange');
       $('.hamburger').addClass('blacken');
    } else {
       $head.removeClass('navchange');
       $('.hamburger').removeClass('blacken');
    }
});