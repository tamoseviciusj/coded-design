<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
</head>
<body>
        <header>
        <nav>
                        <div class="hamburger"></div>
                        <li class="gone"><a href=""><img src="galery/logo.png" alt=""></a></li>
                        <ul>

                            <li class="gone" id="logo"><a href=""><img src="galery/logo.png" alt=""></a></li>
                            <li class="nav"><a href="">Home</a></li>
                            <li><a href="">About</a></li>
                            <li><a href="">Igredients</a></li>
                            <li><a href="">Menu</a></li>
                            <li><a href="">Reviews</a></li>
                            <li><a href="">Reservations</a></li>
                        </ul>
                    </nav>
            <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-inside">
                                <div>
                                    <h1>the right ingredients for the right food</h1>
                                    <div class="center-buttons">
                                        <div>


                                            <div class="header-buttons"><a href="">button</a></div>
                                            <div class="header-buttons"><a href="">button</a></div>
                                            </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
        </header>

        <section class="container">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="about">
                        <div class="about-inside">
                                <h1>Just the right food</h1>
                                <div class="divider"></div>
                                <p>If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!</p>
                                <div class="cook-outside">
                                    <div class="cook"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dish-outside">
                            <div class="dish"></div>
                    </div>
                </div>
            </div>
        </section>

        <div class="ingredients clear">
            <div class="container">
                    <div class="col-lg-6">
                        <p> </p>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="outside-box">
                                <div class="inside-box">
                                    <div class="inside-box-flex">
                                    <h2>Fine ingredients</h2>
                                    <p>If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!                  
                                    </p>
                                        <div class="allproducts">
                                                <div class="products"></div>
                                                <div class="products"></div>
                                                <div class="products"></div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>


        <div class="menu clear">
            <div class="container">

                <div class="row">

                        <div class="col-lg-6 col-md-12">




                                <div class="menus">
                                    <div class="menus-inside">
                                                <h2>Appetisers</h2>
                                    <!-- /////////////////////////////////////////////////////// -->
                                    <?php include 'includes.php';
                                        foreach($app as $dish){
                                            ?>
                                                <div class="dish-menu <?php echo $dish['special']; ?>">
                                                    <div class="dish-inside">
                                                        <div class="dish-price">
                                                            <h3 class="price"> <?php echo $dish['name']; ?></h3>
                                                            <h3 class="price"> <?php echo $dish['price']; ?></h3>
                                                        </div>
            
                                                        <div class="dish-desc">
                                                                <p><?php echo $dish['ingredients']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            }
                                            ?>

                                                <h2>Salads</h2>
                                    <!-- /////////////////////////////////////////////////////// -->
                                    <?php include 'includes.php';
                                        foreach($salads as $dish){
                                            ?>
                                                <div class="dish-menu <?php echo $dish['special']; ?>">
                                                    <div class="dish-inside">
                                                        <div class="dish-price">
                                                            <h3 class="price"> <?php echo $dish['name']; ?></h3>
                                                            <h3 class="price"> <?php echo $dish['price']; ?></h3>
                                                        </div>
            
                                                        <div class="dish-desc">
                                                                <p><?php echo $dish['ingredients']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            }
                                            ?>
            

                                    </div>            
                                </div>
            
                                    
                                    
            
                            </div>


                            <div class="col-lg-6 col-md-12">

                                                                <div class="menus">
                                    <div class="menus-inside">
                                                <h2>Starters</h2>
                                    <!-- /////////////////////////////////////////////////////// -->
                                    <?php include 'includes.php';
                                        foreach($start as $dish){
                                            ?>
                                                <div class="dish-menu <?php echo $dish['special']; ?>">
                                                    <div class="dish-inside">
                                                        <div class="dish-price">
                                                            <h3 class="price"> <?php echo $dish['name']; ?></h3>
                                                            <h3 class="price"> <?php echo $dish['price']; ?></h3>
                                                        </div>
            
                                                        <div class="dish-desc">
                                                                <p><?php echo $dish['ingredients']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            }
                                            ?>

                                                <h2>Main dishes</h2>
                                    <!-- /////////////////////////////////////////////////////// -->
                                    <?php include 'includes.php';
                                        foreach($main as $dish){
                                            ?>
                                                <div class="dish-menu <?php echo $dish['special']; ?>">
                                                    <div class="dish-inside">
                                                        <div class="dish-price">
                                                            <h3 class="price"> <?php echo $dish['name']; ?></h3>
                                                            <h3 class="price"> <?php echo $dish['price']; ?></h3>
                                                        </div>
            
                                                        <div class="dish-desc">
                                                                <p><?php echo $dish['ingredients']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            }
                                            ?>
            

                                    </div>            
                                </div>

                            </div>

                </div>


   



            </div>                
        </div>

        <div class="reviews clear">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    <div class="outside-box">
                                <div class="inside-box">
                                    <div class="inside-box-flex">
                                    <h2>Guest Reviews</h2>
                                    <p>If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!
        
                                    </p>
                                    <p>- food inc, New York</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="booking clear">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-xl-3 remove">
                        <div class="booking-pictures">

                        </div>

                    </div>
                    <div class="col-lg-3 remove">
                        <div class="booking-pictures">
                            
                        </div>
                        
                    </div>
                    <div class="col-lg-6 col-xl-9 col-xm-12">
                        <div class="form">
                            <div class="form-inside">
                                <h2>Just The Right Food</h2>
                                <p>If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food.</p>
                                <p>Delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out! Perfect materials and freshly baked food.</p>
                                <div class="form-bottom">
                                <form action="">
                                    <div class="oneblock">
                                        <div class="wrapper">
                                            <div>
                                            <label for="name">Name</label>
                                        <input class="formstyle" id="name" type="text" placeholder="Name*">
                                            </div>

                                        </div>


                                    </div>
                                    <div class="oneblock clear">
                                        <div class="wrapper">
                                            <div>                                        <label for="email">Email</label>
                                            <input class="formstyle" id="email" type="text" placeholder="Email*">
                                             </div>
                                        </div>
                                    </div>

                                    <div class="twoblock">
                                        <div class="wrapper">
                                            <div>
                                            <label for="date">Date</label>
                                        <input class="formstyle" id="date" type="text" placeholder="Date*">
                                            </div>

                                        </div>

                                    </div>
                                    <div class="twoblock number clear">
                                        <div class="wrapper">
                                            <div>
                                            <label for="number">Party number</label>
                                         <select name="" id="number">
                                        <option value="Number">Number</option>
                                        <option value="Number">Number</option>
                                        <option value="Number">Number</option>
                                        <option value="Number">Number</option>
                                        </select>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="submitoutside">
                                        <input class="submit" type="submit" value="Book now!">
                                    </div>
                                    
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <h3>About us</h3>
                        <p>Lambda's new and expanded Chelsea location represents a truly authentic Greek patisserie, featuring breakfasts of fresh croissants and steaming bowls of café.
                        Lamda the best restaurant in town</p>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <h3>Opening Hours</h3>
                        <p><span>Mon-Thu:</span> 7:00am-8:00pm </p>
                        <p><span>Fri-Sun:</span> 7:00am-10:00pm</p>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <h3>Our Location</h3>
                        <p>19th Paradise Street Sitia</p>
                        <p>128 Meserole Avenue</p>
                    </div>
                </div>
            </div>
        </footer>

  <script src="script.js"></script>
</body>
</html>